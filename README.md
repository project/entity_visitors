## INTRODUCTION
 
This modules provides you with blocks to view the users who visited some entity.
Like a user who visited another user, or a user who visited some content type.
Using this module you can save visits of the users to another profiles or other 
entities like another content type.

There is an advanced option where you can save each visit of each user, 
which can consume a lot of Database space since every visit means new DB record.

There is also another basic option where you can save only last visit per user.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/entity_visitors

 * To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/project/issues/entity_visitors

## REQUIREMENTS

 * If you will be using the mailing functionality then you will 
   need a mailing module like SMTP or SwiftMailer.


## INSTALLATION

 * Install as you would normally install a contributed Drupal module.
   See: https://www.drupal.org/node/895232 for further information.


## CONFIGURATION

 * Excluded Roles: 
   which controls which user role to be counted when they visit an entity.

 * Save the last visits only: 
   if you check this, you get the basic option of saving only the last visits, 
   otherwise you will have a new record for each visiting time.

 * Send mail to the person who was visited: 
   this enables the mailing functionality and asks you to add the template, 
   subject and which user roles to be notified.
   
## UNINSTALLATION
 * you will have to remove the content before uninstalling the module 
   by going to /admin/modules/uninstall/entity/entity_visitors.

## FOR ADMINS
 * you can use admin/structure/entity_visitors to view the list of entities.
 * you can use admin/structure/entity_visitors/settings to add fields.

## MAINTAINERS

 * Ahmed Ayman (midobel) - https://www.drupal.org/u/midobel
 
## Contributors
 * Ravi Kumar Singh (rksyravi) - https://www.drupal.org/u/rksyravi 
 * Toprak Ferman (toprak) - https://www.drupal.org/u/toprak 
 
 ## TODO
 * [ ] Review the security group requirements to verify our module
 * [ ] Make a dashboard block with chartjs.
