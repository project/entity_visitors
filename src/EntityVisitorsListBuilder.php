<?php

namespace Drupal\entity_visitors;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;
use Drupal\Core\Render\Markup;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;

/**
 * Defines a class to build a listing of Entity visitors entities.
 *
 * @ingroup entity_visitors
 */
class EntityVisitorsListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $rendered_message = Markup::create("to delete all the entities click <a href='/admin/modules/uninstall/entity/entity_visitors?destination=/admin/structure/entity_visitors'>here</a>");
    \Drupal::messenger()->addStatus($rendered_message);

    $header['id'] = $this->t('Visited Entity Id');
    $header['name'] = $this->t('Entity Type');
    $header['visitors'] = $this->t('Visited By');
    $header['visiting_time'] = $this->t('Visiting Time');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var \Drupal\entity_visitors\Entity\EntityVisitors $entity */
    $row['id'] = $entity->get('field_visited_entity_id')->getString();
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.entity_visitors.edit_form',
      ['entity_visitors' => $entity->id()]
    );
    $row['visitors'] = $entity->get('field_entity_visitor')->getString();
    $timestamp = $entity->get('field_entity_visiting_time')->getString();
    $visitingTime = new DrupalDateTime();
    $visitingTime->setTimestamp($timestamp);
    $visitingTime = $visitingTime->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT);
    $row['visiting_time'] = $visitingTime;
    return $row + parent::buildRow($entity);
  }

}
