<?php

namespace Drupal\entity_visitors\EventSubscriber;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheableResponseInterface;
use Drupal\entity_visitors\Service\EntityVisitorsManager;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Drupal\Component\Datetime\TimeInterface;

/**
 * A subscriber to the route when it matches an entity route.
 */
class EntityRouteMatchSubscriber implements EventSubscriberInterface {

  /**
   * The manager takes the current visited entity and handle things for you.
   *
   * @var \Drupal\entity_visitors\Service\EntityVisitorsManager
   */
  private $entityVisitedManager;

  /**
   * Time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * {@inheritdoc}
   */
  public function __construct(EntityVisitorsManager $entityVisitedManager, TimeInterface $time) {
    $this->entityVisitedManager = $entityVisitedManager;
    $this->time = $time;
  }

  /**
   * Listen to the requests to check the current route.
   */
  public static function getSubscribedEvents() {
    return [
      KernelEvents::RESPONSE => 'updateEntityVisitors',
    ];
  }

  /**
   * If this was entity view page, then handle this entity.
   */
  public function updateEntityVisitors(FilterResponseEvent $event) {
    // Check https://www.lullabot.com/articles/common-max-age-pitfalls-with-drupal-cache
    if (!$event->isMasterRequest()) {
      return;
    }
    $response = $event->getResponse();
    if (!($response instanceof CacheableResponseInterface)) {
      return;
    }
    $max_age = (int) $response->getCacheableMetadata()->getCacheMaxAge();
    if ($max_age !== Cache::PERMANENT) {
      $routeName = $event->getRequest()->get('_route');
      // Match the route with an entity view route.
      if (preg_match('/(entity.)*(.canonical)/', $routeName)) {
        // eg, node, user, etc,.
        $visitedEntityType = explode('.', $routeName)[1];
        $visitedEntityId = $event->getRequest()->get($visitedEntityType)->id();

        $this->entityVisitedManager->handleEntity($routeName, $visitedEntityType, $visitedEntityId);
      }
      $response->setMaxAge($max_age);
      $date = new \DateTime('@' . ($this->time->getRequestTime() + $max_age));
      $response->setExpires($date);
    }
  }

}
