<?php

namespace Drupal\entity_visitors\EventSubscriber;

use Drupal\Core\Config\ConfigManagerInterface;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\entity_visitors\Event\EntityVisitedEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * A subscriber to the route when it matches an entity route.
 */
class SendMailOnEntityVisitedSubscriber implements EventSubscriberInterface {

  /**
   * A mail manager instance.
   *
   * @var \Drupal\Core\Mail\MailManagerInterface
   */
  private $mailManager;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * A configuration manager instance.
   *
   * @var \Drupal\Core\Config\ConfigManagerInterface
   */
  private $configManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(MailManagerInterface $mailManager, ConfigManagerInterface $configManager, EntityTypeManagerInterface $entityTypeManager) {
    $this->mailManager = $mailManager;
    $this->configManager = $configManager;
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * Listen to the requests to check the current route.
   */
  public static function getSubscribedEvents() {
    return [
      EntityVisitedEvent::VISITED => 'sendMail',
    ];
  }

  /**
   * Send mail on entity visited.
   */
  public function sendMail(EntityVisitedEvent $event) {
    if ($event->visitedEntityType == "user") {
      $user = $this->entityTypeManager->getStorage('user')->load($event->visitedEntityId);
      $allowedRoles = $this->configManager->getConfigFactory()
        ->get('entity_visitors.entity_visitors_config')
        ->get('enable_mail_roles');
      $sendMailEnabled = $this->configManager->getConfigFactory()
        ->get('entity_visitors.entity_visitors_config')
        ->get('send_mail_on_visit');
      if ($sendMailEnabled && !empty($allowedRoles) && array_intersect($user->getRoles(), $allowedRoles)) {
        $params = [
          "subject" => $this->configManager->getConfigFactory()
            ->get('entity_visitors.entity_visitors_config')
            ->get('mail_subject'),
          "body" => $this->configManager->getConfigFactory()
            ->get('entity_visitors.entity_visitors_config')
            ->get('mail_template')["value"],
        ];
        $this->mailManager->mail("entity_visitors", "mail_to_visited_user", $user->getEmail(), $user->getPreferredLangcode(), $params);
      }
    }
  }

}
