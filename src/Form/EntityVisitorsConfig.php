<?php

namespace Drupal\entity_visitors\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class EntityVisitiorsConfig.
 */
class EntityVisitorsConfig extends ConfigFormBase {

  /**
   * The RequestStack object.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('request_stack')
    );
  }

  /**
   * EntityVisitorsConfig constructor.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   *   The request service.
   */
  public function __construct(RequestStack $requestStack) {
    $this->requestStack = $requestStack;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'entity_visitors.entity_visitors_config',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'entity_visitiors_config';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('entity_visitors.entity_visitors_config');
    $form['excluded_roles'] = [
      '#type' => 'select',
      '#multiple' => TRUE,
      '#description' => $this->t("Excluding a role means that you won't count it's visits to the entities."),
      '#title' => $this->t('Excluded Roles'),
      '#options' => user_role_names(),
      '#default_value' => $config->get('excluded_roles') ?: [],
    ];
    $form['save_last_visits_only'] = [
      '#type' => 'checkbox',
      '#description' => $this->t("Save only the last visits per each user, hence you won't get a history of every visiting time."),
      '#title' => $this->t('Save the last visits only.'),
      '#default_value' => $config->get('save_last_visits_only') !== NULL ? $config->get('save_last_visits_only') : TRUE,
    ];
    $form['send_mail_on_visit'] = [
      '#type' => 'checkbox',
      '#title' => $this->t("Send mail to the person who was visited."),
      '#description' => $this->t("When you visit a profile, they will be notified that someone has visited their profile."),
      '#default_value' => $config->get('send_mail_on_visit') !== NULL ? $config->get('send_mail_on_visit') : FALSE,

    ];
    $url = $this->requestStack->getUri() . "/user/login?destination=/user";
    $form['mail_subject'] = [
      '#type' => 'textfield',
      '#title' => 'Mail subject',
      '#states' => [
        // Show this textfield only if the radio 'other' is selected above.
        'visible' => [
          ':input[name="send_mail_on_visit"]' => ['checked' => TRUE],
        ],
      ],
      '#default_value' => $config->get('mail_subject') ? $config->get('mail_subject') :
      "Someone has visited your profile",
    ];
    $form['mail_template'] = [
      '#type' => 'text_format',
      '#states' => [
        // Show this textfield only if the radio 'other' is selected above.
        'visible' => [
          ':input[name="send_mail_on_visit"]' => ['checked' => TRUE],
        ],
      ],
      '#title' => $this->t("The body text to be sent."),
      '#description' => $this->t("Add a descriptive body text to you users."),
      '#default_value' => $config->get('mail_template') ? $config->get('mail_template')['value'] :
      "Someone has visited your profile, please log in to your <a href='$url'>profile</a> for more information.",
    ];
    $form['enable_mail_roles'] = [
      '#type' => 'select',
      '#multiple' => TRUE,
      '#title' => $this->t('Specify mail receiver roles'),
      '#description' => $this->t("Specify the roles that should receive mails ."),
      '#options' => user_role_names(),
      '#default_value' => $config->get('enable_mail_roles') ?: [],
      '#states' => [
        // Show this textfield only if the radio 'other' is selected above.
        'visible' => [
          ':input[name="send_mail_on_visit"]' => ['checked' => TRUE],
        ],
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('entity_visitors.entity_visitors_config')
      ->set('excluded_roles', $form_state->getValue('excluded_roles'))
      ->set('save_last_visits_only', $form_state->getValue('save_last_visits_only'))
      ->set('send_mail_on_visit', $form_state->getValue('send_mail_on_visit'))
      ->set('mail_subject', $form_state->getValue('mail_subject'))
      ->set('mail_template', $form_state->getValue('mail_template'))
      ->set('enable_mail_roles', $form_state->getValue('enable_mail_roles'))
      ->save();
    $this->messenger()->addMessage($this->t("Saved the configurations."));
  }

}
